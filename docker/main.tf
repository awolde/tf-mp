# a simple module that creates a vm in a subnet, so many defaults assumed

variable "project_id" {}
variable "vpc_name" {
  default = "default"
}
variable "region" {
  default = "us-central1"
}
//variable "bucket" {}

resource "google_compute_instance" "test_vm" {
  name         = "test-vm"
  project      = "${var.project_id}"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  scratch_disk {}

  network_interface {
    subnetwork = "projects/${var.project_id}/regions/${var.region}/subnetworks/${var.vpc_name}"
    access_config {}
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
}

terraform {
  required_version = ">= 0.11.10"

  backend "gcs" {
    //set by -backend-config cli parameter like this:
    //-backend-config="bucket=mw-acc-tfstate-dev" -backend-config="prefix=from-docker"
  }
}
