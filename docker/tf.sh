#!/usr/bin/env bash
set -e
BUCKET=$1
PREFIX=$2
PROJ=$3
RUN=$4
if [ $# -lt 3 ]; then
  echo "Usage: $0 bucket-name prefix project"
else
  terraform init -backend-config="bucket=${BUCKET}" -backend-config="prefix=${PREFIX}"
  terraform plan -var="project_id=${PROJ}"
  if [ -z $RUN ]; then
      echo "Apply not specified, exiting ;)"
  else
    if [ $RUN == "apply" ]; then
      echo "Apply detected..... applying!!!"
      terraform apply -auto-approve -var="project_id=${PROJ}"
    else if [ $RUN == "destroy" ]; then
      echo "Destruction on the way!!!"
      terraform destroy -auto-approve -var="project_id=${PROJ}"
      fi
    fi
  fi
fi
