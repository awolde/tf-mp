## What it is
This repo builds a Docker image with Terraform installed in it and with a
sample main.tf file that creates a vm in the default VPC in US-central1 region.
To make things so much easy, the terraform script for now only expects a project id,
GCS bucket name where the state file is going to be stored and prefix (sub-dir) inside the bucket.

### How to
- Launch CoreOs VM in GCE

- Clone this repo and build the image

```docker build -t tf .```

- Plan it

```docker run -ti tf state-file-bucket-name prefix project-id```

- Apply it

```docker run -ti tf state-file-bucket-name prefix project-id apply```

- Destroy it

```docker run -ti tf state-file-bucket-name prefix project-id destroy```


CoreOS should pass the service account creds to the container running Terraform. Make sure
that service account has enough permissions to create a vm and read/write on the state file bucket.

## Example running it CoreOs
```
name=coreos; 
gcloud compute instances create-with-container $name \
  --zone us-central1-a --service-account your-sservice-account@project-id.iam.gserviceaccount.com \
  --scopes storage-rw,compute-rw \
  --container-image gcr.io/your-cloud-build-image-name \
  --container-arg="state-bucket" --container-arg="my-prefix"  --container-arg="project-id" --container-arg="destroy"
```
TODO:

Supply a gcloud script to setup service account and bucket.
